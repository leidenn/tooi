from nltk.book import *

def main():
    print("******************** 9 ********************")
    s = "This is testing string"
    print("using just s")
    print("'" + s + "'")
    print("using print")
    print(s)
    print("We can use (s + ' ')*n")
    print((s + ' ') * 3)
    print("******************** 10 ********************")
    my_sent = ['This', 'is', 'testing', 'string']
    my_join_string = ' '.join(my_sent)
    print('join = ' + my_join_string)
    print(my_join_string.split())
    print("******************** 11 ********************")
    p1 = "test with test"
    p2 = "another house"
    p3 = "interesting thing"
    print("p1 + p2 = " + p1 + p2)
    print("p1 + p3 = " + p1 + p3)
    print("p2 + p3 = " + p2 + p3)
    print("p2 + p1 + p3 = " + p2  + p1 + p3)
    print("len(p1 + p2) = " + str(len(p1 + p2)) + " = len(p1) + len(p2) = " + str(len(p1) + len(p2)))
    print("******************** 12 ********************")
    print("""
    a. "Monty Python"[6:12]
    b. ["Monty", "Python"][1]
    Эти два выражания обладают одинаковым значение Python. Второе будет лучше.
    Программист сразу может видеть какой вариант будет выбран. И идеологически это верно
    """)
    print("******************** 13 ********************")
    print("sent1 = " + ' '.join(sent1))
    text1.generate("")


if __name__ == "__main__":
    main()
