import json


def printDict(dict):
    print(json.dumps(dict, indent=4))


if __name__ == '__main__':
    scenario = json.loads(open("RomeoAndJuliet.json").read())

    repl = dict()
    for act in scenario["acts"]:
        for scene in act["scenes"]:
            for action in scene["action"]:
                character = action["character"]
                old = repl.get(character)
                says = action["says"]
                if old is not None:
                    old.append(says)
                else:
                    repl.update([(character, [says])])

    ################################################################
    maxRepl = dict.fromkeys(repl.keys())
    for name, says in repl.items():
        maxRepl[name] = len(says)
    printDict(maxRepl)
    print(max(maxRepl, key=maxRepl.get))
    ################################################################
    maxReplLen = dict.fromkeys(repl.keys())
    for name, says_list in repl.items():
        max_l = 0
        for says in says_list:
            l = 0
            for say in says:
                l += len(say)
            if l > max_l:
                max_l = l
        maxReplLen[name] = max_l
    printDict(maxReplLen)
    print(max(maxReplLen, key=maxReplLen.get),)
    ################################################################
    f = open("end.json", "w")
    json.dump([1, 2, 3.3, "test", {"field1": 3, "field2": "str"}], f, indent=4)
