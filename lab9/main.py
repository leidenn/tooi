import random
import datetime
from math import floor


class Figure:
    def __init__(self, color):
        self.color = color


class Circle(Figure):
    def __init__(self, r, color):
        super().__init__(color)
        self.r = r

    def __str__(self):
        return f"Circle(r={self.r}, color={self.color})"


class Rectangle(Figure):
    def __init__(self, a, b, color):
        super().__init__(color)
        self.a, self.b = a, b

    def __str__(self):
        return f"Rectangle(a={self.a}, b={self.b}, color={self.color})"


class Box:
    def __init__(self):
        self.__list = []

    def __add__(self, other):
        if isinstance(other, Figure):
            self.__list.append(other)

    def __str__(self):
        if len(self.__list) == 0:
            return "Box is empty"
        else:
            return ", ".join([f.__str__() for f in self.__list])


class Seller:
    def __init__(self, price):
        self.__price = price
        self.__money = 0

    def accept_payment(self, money):
        self.__money += money

    def get_price(self):
        return self.__price

    def get_ticket(self, money):
        if money < self.__price:
            return None
        else:
            hour = floor(random.random() * 24)
            minute = floor(random.random() * 60)
            return Ticket(random.randint(0, 1000),
                          datetime.time(hour=hour, minute=0),
                          datetime.time(hour=hour, minute=minute))


class Person:
    def __init__(self, money):
        self.tickets = list()
        self.money = money

    def buy_ticket(self, seller):
        result = seller.get_ticket(self.money)
        if result:
            self.tickets.append(result)
            self.transfer_money(seller, seller.get_price())
            return True
        else:
            return False

    def get_tickets(self):
        return self.tickets

    def transfer_money(self, seller, money):
        if self.money >= money:
            seller.accept_payment(money)
            self.money -= money


class Ticket:
    def __init__(self, train_id, departure_time, arrival_time):
        self.train_id = train_id
        self.departure_time = departure_time
        self.arrival_time = arrival_time

    def __str__(self):
        return f'Ticket for train #{self.train_id}.' \
            f'From {self.departure_time} to {self.arrival_time}'


if __name__ == '__main__':
    a = Circle(2, 2)
    print(a)
    b = Rectangle(1, 1, 1)
    print(b)
    box = Box()
    print(box)
    box + a
    print(box)
    box + b
    print(box)

    # <><><><><><><><><><><><><><><><><><><><>
    #                  2
    # <><><><><><><><><><><><><><><><><><><><>

    station = Seller(price=51)
    me = Person(money=1000)
    while me.buy_ticket(station):
        pass
    [print(ticket) for ticket in me.tickets]
