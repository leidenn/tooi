import opencorpora
import itertools


class Corpus:
    def __init__(self, path):
        self.__corpus = opencorpora.CorpusReader(path)
        a = itertools.islice(self.__corpus.iter_documents(), 2)

        self.sentences = list()
        for i in a:
            for sent in i.iter_parsed_sents():
                s = Sentence(sent)
                print(s)
                self.sentences.append(Sentence(sent))

    def __str__(self):
        return f"Corpus({[sent.__str__() for sent in self.sentences]})"


class Sentence:
    """ Generate new Word from sentence iterator (which is generator, too )"""

    def __iter_words(self):
        for word in self.__raw_sents_iter:
            # print(word)

            b = Word(word[0], [l[0] for l in word[1]], [l[1] for l in word[1]])
            self.words.append(b)

    def __init__(self, iter):
        self.__raw_sents_iter = iter
        self.words = list()
        self.__iter_words()

    def __str__(self):
        return f"Sentence({[word.__str__() for word in self.words]})"


class Word:
    def __init__(self, form, lemma, grammems):
        self.form = form
        self.lemma = lemma
        self.grammems = grammems

    def __str__(self):
        return f"Word({self.form}, {self.lemma}, {self.grammems})"


class Int:
    arg = None  # int


class Char:
    arg = None  # char

    def add_to(self, other):
        other.arg += ord(self.arg)


class Student:
    def __init__(self, student_id, group, phone_number=None):
        self.student_id = student_id
        self.group = group
        if phone_number is not None:
            self.phone_number = phone_number


if __name__ == '__main__':
    # 1. Сделать два класса, два объекта. Один изменяет другой
    a = Int()
    a.arg = 2

    b = Char()
    b.arg = 'c'
    b.add_to(a)
    print(a.arg)

    # 2. Создать класс с конструктором. Сгенерировать объекты различными способами

    student1 = Student(0, 16208)
    student2 = Student(1, 16202)
    student3 = Student(3, 16208, 8952)

    # 3. OpenCorpora into class
    c = Corpus('annot.opcorpora.xml')
    print(c)
