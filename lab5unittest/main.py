import unittest
import random
from enum import Enum


def return_true():
    return True


def generate_pair(a, b):
    return [a, b]


def inc(a):
    return a + 1


class Token(Enum):
    NUMBER = 0
    DATE_WORD = 1
    USELESS = 2


def have_date(str):
    date_word = [
        'год', 'году', 'года', 'г', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа',
        'сентября',
        'октября', 'ноября', 'декабря', 'лет', 'век', 'веке'
    ]
    str = str.split(' ')
    t = []
    for word in str:
        if word.isdigit():
            t.append(Token.NUMBER)
        elif word.lower() in date_word:
            t.append(Token.DATE_WORD)
        else:
            t.append(Token.USELESS)
    for i in range(0, len(t)):
        if t[i] == Token.NUMBER and t[i + 1] == Token.DATE_WORD:
            return True


class TestFuncs(unittest.TestCase):

    def test_return_True(self):
        self.assertTrue(return_true(), "func must return True")
        self.assertFalse(not return_true(), "func must return True")

    def test_generate_pair(self):
        self.assertIn(4, generate_pair(4, 6), "func must generate list with 4 and 6")
        self.assertNotIn(5, generate_pair(4, 6), "func must generate list with 4 and 6")
        self.assertCountEqual((2, 3), generate_pair(2, 3))

    def test_inc(self):
        self.assertGreater(inc(2), 2)
        self.assertLess(2, inc(2))

    def test_subtests(self):
        for i in range(0, 9):
            with self.subTest(i=i):
                self.assertGreaterEqual(random.randint(0, 100) / 100, 0.5)

    def test_data(self):
        strs = [
            'с тех пор прошло 3 года',
            'бюджет в 2018 году',
            'в 6 веке',
            '5 марта',
            'почти половина мужчин не доживает до 65 лет',
            'в 1987 г',
            'в мае 2018 года'
        ]
        for str in strs:
            with self.subTest(i=str):
                self.assertTrue(have_date(str))


if __name__ == '__main__':
    var = unittest.main
    # print(have_date('я родился в 1990 году'))
