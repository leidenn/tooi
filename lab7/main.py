import time
import datetime

if __name__ == '__main__':
    while True:
        t = int(input("Введите кол-во сек."))
        if t < 0:
            break
        time.sleep(t)
    # <><><><><><>
    start = time.time()
    while t < 10000000:
        t += 1
    end = time.time()
    print(f"Прошло времени {end - start}")
    file = open("{:%B %d, %Y %H:%M:%S}.txt".format(datetime.datetime.now()), "w")
    file.write(str(end - start))
    file.close()
    # <><><><><><>
    date = input("Enter date yyyy-mm-dd\n")
    birth_date = datetime.datetime.strptime(date, '%Y-%m-%d')
    t = datetime.date.today()- birth_date.date()
    print(t.days, " days passed\n")
