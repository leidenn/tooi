import xml.etree.ElementTree as et

if __name__ == '__main__':
    tree = et.parse("annot.opcorpora.no_ambig.xml")
    root = tree.getroot()
    for text in root:
        for paragraph in text.iter("paragraphs"):
            for sentence in paragraph.iter("sentence"):
                print(sentence.get("source"))
