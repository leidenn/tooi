import csv

if __name__ == '__main__':
    with open('stage3_test.csv') as csvfile:
        print('out1...')
        reader = csv.DictReader(csvfile)
        with open('out1.csv', 'w') as out:
            writer = csv.DictWriter(out, fieldnames=reader.fieldnames)
            writer.writeheader()
            for row in reader:
                if len(row['Images'].split(',')) >= 3:
                    writer.writerow(row)
        print('out1 done.')

    with open('stage3_test.csv') as csvfile:
        print('out2...')
        reader = csv.DictReader(csvfile)
        with open('out2.csv', 'w') as out:
            writer = csv.DictWriter(out, fieldnames=reader.fieldnames)
            writer.writeheader()
            for row in reader:
                if 10000 < float(row['Price']) < 50000:
                    writer.writerow(row)
        print('out2 done.')

    with open('stage3_test.csv') as csvfile:
        print('out3...')
        reader = csv.DictReader(csvfile)
        with open('out3.csv', 'w') as out:
            fieldnames = reader.fieldnames
            writer = csv.DictWriter(out, fieldnames=["Id", "Title", "Price"])
            writer.writeheader()
            for row in reader:
                row.pop('Images')
                row.pop('Description')
                print(row.keys())
                writer.writerow(row)
        print('out3 done.')
